from matplotlib import pyplot as plt
import numpy as np
from scipy import stats

import src.config as config
import src.utils as utils
import src.parzen as parzen

# b) Estime las diferentes F X (x) utilizando Parzen windows con ventanas según se pide.


def plot(
    name, title, support, f1_theoretical, f1_estimated, f2_theoretical, f2_estimated
):
    plt.figure(name)
    plt.title(title)
    plt.plot(support, f1_theoretical, linestyle="dotted", label="$F_{X_1}(x)$ teórico")
    plt.plot(support, f2_theoretical, linestyle="dotted", label="$F_{X_2}(x)$ teórico")
    plt.plot(support, f1_estimated, label="$\hat{F}_{X_1}(x)$ estimado")
    plt.plot(support, f2_estimated, label="$\hat{F}_{X_2}(x)$ estimado")
    plt.xlabel("Soporte")
    plt.legend()
    plt.grid(which="major", color="#DDDDDD", linewidth=0.8)
    plt.grid(which="minor", color="#EEEEEE", linestyle=":", linewidth=0.5)
    plt.minorticks_on()
    plt.show()
    plt.pause(0.2)


def compare_window_size(h_list, support, cfg):
    name = cfg["name"]
    n_samples = cfg["n_samples"]
    f1, f2 = cfg["f1"], cfg["f2"]
    a, b = f1["a"], f1["b"]
    mu, sigma = f2["mu"], f2["sigma"]

    x1_samples, x2_samples = utils.gen_samples(n_samples, f1=f1, f2=f2)

    f1_theoretical = stats.uniform.pdf(support, a, b - a)
    f2_theoretical = stats.norm.pdf(support, mu, sigma)

    _, (ax1, ax2) = plt.subplots(
        nrows=1, ncols=2, figsize=(14, 4), num=f"b_parzen_estimacion_{name}_h_compare"
    )

    ax1.set_title(f"Uniforme [{a}, {b}]")

    ax1.plot(support, f1_theoretical, linestyle="dotted", label="$F_{X_1}(x)$ teórico")

    ax1.set_xlabel("Soporte")
    ax1.legend()
    ax1.grid(which="major", color="#DDDDDD", linewidth=0.8)
    ax1.grid(which="minor", color="#EEEEEE", linestyle=":", linewidth=0.5)
    ax1.minorticks_on()

    ax2.set_title(f"Gaussiana ({mu}, {sigma**2})")
    ax2.plot(support, f2_theoretical, linestyle="dotted", label="$F_{X_2}(x)$ teórico")
    ax2.yaxis.tick_right()

    ax2.set_xlabel("Soporte")
    ax2.legend()
    ax2.grid(which="major", color="#DDDDDD", linewidth=0.8)
    ax2.grid(which="minor", color="#EEEEEE", linestyle=":", linewidth=0.5)
    ax2.minorticks_on()
    plt.ion()
    plt.show()
    plt.pause(0.1)
    for h in h_list:
        f1_estimated = parzen.estimate(support, x1_samples, h)
        ax1.plot(
            support, f1_estimated, label=f"$\hat{{F}}_{{X_1}}(x)$ estimado con h={h}"
        )
        ax1.legend()
        plt.draw()
        plt.pause(0.1)

        f2_estimated = parzen.estimate(support, x2_samples, h)
        ax2.plot(
            support, f2_estimated, label=f"$\hat{{F}}_{{X_2}}(x)$ estimado con h={h}"
        )
        ax2.legend()

        plt.draw()
        plt.pause(0.1)
    plt.ioff()


def estimate(h, support, cfg):
    name = cfg["name"]
    n_samples = cfg["n_samples"]
    f1, f2 = cfg["f1"], cfg["f2"]
    a, b = f1["a"], f1["b"]
    mu, sigma = f2["mu"], f2["sigma"]

    x1_samples, x2_samples = utils.gen_samples(n_samples, f1=f1, f2=f2)

    f1_estimated = parzen.estimate(support, x1_samples, h)
    f1_theoretical = stats.uniform.pdf(support, a, b - a)

    f2_estimated = parzen.estimate(support, x2_samples, h)
    f2_theoretical = stats.norm.pdf(support, mu, sigma)

    plot(
        name=f"b_parzen_estimacion_{name}_h_{h}",
        title=f"Uniforme [{a}, {b}] y Gaussiana ({mu}, {sigma**2}) (h = {h})",
        support=support,
        f1_theoretical=f1_theoretical,
        f1_estimated=f1_estimated,
        f2_theoretical=f2_theoretical,
        f2_estimated=f2_estimated,
    )


if __name__ == "__main__":
    plt.ion()

    h_list = [0.6, 1.1, 1.6]
    support = np.linspace(-6, 11, 100)

    # tema 3)
    # compare_window_size(h_list, support, config.t3)

    # tema 4)
    # compare_window_size(h_list, support, config.t4)

    h = 1.1
    # tema 3)
    estimate(h, support, config.t3)

    # tema 4)
    estimate(h, support, config.t4)

    plt.ioff()
    plt.show()
