from matplotlib import pyplot as plt
import numpy as np
from scipy import stats

import src.config as config
import src.utils as utils
import src.parzen as parzen
import src.knn as knn

# d) Para b) y c) realice un clafificador y clasifique 10^2 nuevas muestras, mida el error obtenido.


def classify_all(h, k_list, cfg):
    n_samples = cfg["n_samples"]
    f1_params, f2_params = cfg["f1"], cfg["f2"]

    # genero muestras de prueba y entrenamiento
    n_test = int(1e2)
    samples, real_labels = utils.gen_mixed_samples(n_test, f1_params, f2_params)
    train_samples_1, train_samples_2 = utils.gen_samples(
        n_samples, f1_params, f2_params
    )

    # estimadores parzen
    parzen_f1_est = parzen.estimate(samples, train_samples_1, h)
    parzen_f2_est = parzen.estimate(samples, train_samples_2, h)
    classify(
        f"parzen_h_{h}",
        f"Parzen (h = {h})",
        samples,
        real_labels,
        parzen_f1_est,
        parzen_f2_est,
        cfg,
    )

    for k in k_list:
        # estimadores knn
        knn_f1_est = knn.estimate(samples, train_samples_1, k)
        knn_f2_est = knn.estimate(samples, train_samples_2, k)
        classify(
            f"knn_k_{k}",
            f"KNN (k = {k})",
            samples,
            real_labels,
            knn_f1_est,
            knn_f2_est,
            cfg,
        )


def classify(id, name, samples, real_labels, f1_est, f2_est, cfg):
    # clasifico y obtengo error
    f1_params, f2_params = cfg["f1"], cfg["f2"]
    est_labels = utils.bayes_classify(f1_est, f1_params["p"], f2_est, f2_params["p"])
    est_prob_e = utils.get_error_probability(
        est_labels, real_labels, f1_params["p"], f2_params["p"]
    )
    print(f"{name} estimator error rate : {est_prob_e:.2%}")

    # grafico resultados
    plot_results(
        f"d_estimator_error_rates_{cfg['name']}_{id}",
        f"Clasificación con Estimador {name}",
        samples,
        real_labels,
        est_labels,
        f1_est,
        f2_est,
        cfg,
    )


def plot_results(fig, title, samples, real_labels, est_labels, f1_est, f2_est, cfg):
    f1_params, f2_params = cfg["f1"], cfg["f2"]
    plt.figure(fig)
    plt.title(title)

    p1, p2 = f1_params["p"], f2_params["p"]
    a, b = f1_params["a"], f1_params["b"]
    mu, sigma = f2_params["mu"], f2_params["sigma"]

    support = np.linspace(-6, 11, 200)
    f1_theoretical = stats.uniform.pdf(support, a, b - a)
    f2_theoretical = stats.norm.pdf(support, mu, sigma)

    samples_1, real_idx_1 = utils.filter_by_label(samples, real_labels, ref=0)
    samples_2, real_idx_2 = utils.filter_by_label(samples, real_labels, ref=1)

    est_samples_1, est_idx_1 = utils.filter_by_label(samples, est_labels, ref=0)
    est_samples_2, est_idx_2 = utils.filter_by_label(samples, est_labels, ref=1)

    plt.plot(
        support,
        f1_theoretical * p1,
        linestyle="dotted",
        label="$F_{X_1}(x).P(w_1)$ teórico",
    )
    plt.plot(
        support,
        f2_theoretical * p2,
        linestyle="dotted",
        label="$F_{X_2}(x).P(w_2)$ teórico",
    )
    plt.scatter(
        samples_1 + samples_2,
        utils.filter_and_scale(f1_est, real_idx_1, p1)
        + utils.filter_and_scale(f2_est, real_idx_2, p2),
        color="green",
        alpha=0.6,
        label="Etiqueta real",
    )
    plt.scatter(
        est_samples_1,
        utils.filter_and_scale(f1_est, est_idx_1, p1),
        marker="x",
        label="Marcado como ${W_1}$",
    )
    plt.scatter(
        est_samples_2,
        utils.filter_and_scale(f2_est, est_idx_2, p2),
        marker="x",
        label="Marcado como ${W_2}$",
    )

    plt.legend()
    plt.grid(which="major", color="#DDDDDD", linewidth=0.8)
    plt.grid(which="minor", color="#EEEEEE", linestyle=":", linewidth=0.5)
    plt.minorticks_on()
    plt.yticks([])
    plt.show()


if __name__ == "__main__":
    h = 1.1
    k_list = [1, 10, 50, 100]

    # tema 3)
    classify_all(h, k_list, config.t3)

    # tema 4)
    classify_all(h, k_list, config.t4)
