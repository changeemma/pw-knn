import numpy as np


def gen_samples(n_samples, f1, f2):
    ## Uniforme [a, b]
    a, b = f1["a"], f1["b"]
    x1_samples = np.random.uniform(low=a, high=b, size=n_samples)

    ## Gaussiana (mu, sigma)
    mu, sigma = f2["mu"], f2["sigma"]
    x2_samples = np.random.normal(loc=mu, scale=sigma, size=n_samples)

    return x1_samples, x2_samples


def gen_mixed_samples(n, f1, f2):
    # parametros de las distribuciones
    p1, a, b = f1["p"], f1["a"], f1["b"]
    p2, mu, sigma = f2["p"], f2["mu"], f2["sigma"]

    # generadores de muestras
    distr = [
        # Uniforme [a, b]
        (p1, lambda: np.random.uniform(low=a, high=b)),
        # Gaussiana (mu, sigma)
        (p2, lambda: np.random.normal(loc=mu, scale=sigma)),
    ]
    # genero de manera aleatoria segun p1 y p2
    labels = [w for w in np.random.choice(len(distr), p=[p for p, _ in distr], size=n)]
    samples = [distr[w][1]() for w in labels]

    return samples, labels


# devuelve un listado de 0s y 1s segun 'p1' o 'p2' ponderados por sus probabilidades
# a priori 'pap1' y 'pap2' respectivamente
def bayes_classify(p1, pap1, p2, pap2):
    w1, w2 = 0, 1

    def _classify(p1_i, p2_i):
        return w1 if p1_i * pap1 > p2_i * pap2 else w2

    return [_classify(p1_i, p2_i) for p1_i, p2_i in zip(p1, p2)]


# devuelve la probabilidad de error
def get_error_probability(x, ref, p1, p2):
    w1, w2 = 0, 1
    # obtengo total por clase
    n1, n2 = ref.count(w1), ref.count(w2)
    # obtengo el indice de error por cada clase 
    err1 = sum([1 / n1 if ri == w1 and xi != ri else 0 for xi, ri in zip(x, ref)])
    err2 = sum([1 / n2 if ri == w2 and xi != ri else 0 for xi, ri in zip(x, ref)])
    # pondero con la probabilidad a priori
    return err1 * p1 + err2 * p2


# devuelve un listado de las muestras e índice con label 'ref'
def filter_by_label(samples, labels, ref):
    filtered_labels = [i for i, label in enumerate(labels) if label == ref]
    return [samples[i] for i in filtered_labels], filtered_labels


# devuelve los elementos de 'f' con índice 'idx' escalados con 'p'
def filter_and_scale(f, idx, p):
    return list(map(lambda i: f[i] * p, idx))
