from matplotlib import pyplot as plt
import numpy as np
from scipy import stats

import src.config as config
import src.utils as utils
import src.knn as knn

# Estime las diferentes F X (x) utilizando Kn vecinos más cercanos para diferentes valores de k = 1, 10, 50 y 100.


def plot(
    name, title, support, f1_theoretical, f1_estimated, f2_theoretical, f2_estimated
):
    plt.figure(name)
    plt.title(title)
    plt.plot(support, f1_theoretical, linestyle="dotted", label="$F_{X_1}(x)$ teórico")
    plt.plot(support, f2_theoretical, linestyle="dotted", label="$F_{X_2}(x)$ teórico")
    plt.plot(support, f1_estimated, label="$\hat{F}_{X_1}(x)$ estimado")
    plt.plot(support, f2_estimated, label="$\hat{F}_{X_2}(x)$ estimado")
    plt.xlabel("Soporte")
    plt.legend()
    plt.grid(which="major", color="#DDDDDD", linewidth=0.8)
    plt.grid(which="minor", color="#EEEEEE", linestyle=":", linewidth=0.5)
    plt.minorticks_on()
    plt.show()
    plt.pause(0.2)


def estimate(k, support, cfg):
    name = cfg["name"]
    n_samples = cfg["n_samples"]
    f1, f2 = cfg["f1"], cfg["f2"]
    a, b = f1["a"], f1["b"]
    mu, sigma = f2["mu"], f2["sigma"]

    samples_w1, samples_w2 = utils.gen_samples(n_samples, f1=f1, f2=f2)

    f1_estimated = knn.estimate(support, samples_w1, k)
    f1_theoretical = stats.uniform.pdf(support, a, b - a)

    f2_estimated = knn.estimate(support, samples_w2, k)
    f2_theoretical = stats.norm.pdf(support, mu, sigma)

    plot(
        name=f"c_knn_estimacion_{name}_k_{k}",
        title=f"Uniforme [{a}, {b}] y Gaussiana ({mu}, {sigma**2}) (k = {k})",
        support=support,
        f1_theoretical=f1_theoretical,
        f1_estimated=f1_estimated,
        f2_theoretical=f2_theoretical,
        f2_estimated=f2_estimated,
    )


if __name__ == "__main__":
    k_list = [1, 10, 50, 100]
    support = np.linspace(-6, 11, 100)
    plt.ion()

    # tema 3)
    for k in k_list:
        estimate(k, support, config.t3)

    # tema 4)
    for k in k_list:
        estimate(k, support, config.t4)

    plt.ioff()
    plt.show()
