import numpy as np
from scipy import stats
from tqdm import tqdm  # progress bar

# estima la densidad de probabilidad de 'x' mediante ventanas de parzen
def estimate(x, trn_x, h):
    a = len(trn_x) * h

    def _estimate(xi):
        return sum([window_function((xi - trn_xi) / h, h) for trn_xi in trn_x]) / a

    return [_estimate(xi) for xi in tqdm(x, desc="[info] parzen estimation")]


# función ventana gaussiana
def window_function(x, h):
    mu = 0  # mu = 0, centrada en las muestras
    sd = h / 6  # 3*desvios de mu  => sigma = h/(2*3)
    return stats.norm.pdf(x, mu, sd) if np.abs(x) <= 0.5 else 0
