t3 = {
    "n_samples": int(1e4),
    "name": "T3",
    "f1": {  # uniforme [2, 10]: a = 2, b = 10
        "p": 0.7,
        "a": 2,
        "b": 10,
    },
    "f2": {  # gaussiana (2, 4): mu = 2, var = 4 (sigma = 2)
        "p": 0.3,
        "mu": 2,
        "sigma": 2,
    },
}

t4 = {
    "n_samples": int(1e4),
    "name": "T4",
    "f1": {  # uniforme [0, 10]: a = 0, b = 10
        "p": 0.3,
        "a": 0,
        "b": 10,
    },
    "f2": {  # gaussiana (2, 4): mu = 2, var = 4 (sigma = 2)
        "p": 0.7,
        "mu": 2,
        "sigma": 2,
    },
}
