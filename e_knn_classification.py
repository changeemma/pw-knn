import src.config as config
import src.utils as utils
import src.knn as knn

# e) Implemente la regla de clasificación del K vecino más cercano para $K = 1, 11, 51$ y calcule el error al clasificar las mismas muestras que en d).


def classify_all(k_list, cfg):
    n_samples = cfg["n_samples"]
    f1, f2 = cfg["f1"], cfg["f2"]

    # genero muestras de prueba y entrenamiento
    n_test = int(1e2)
    samples, real_labels = utils.gen_mixed_samples(n_test, f1, f2)
    train_samples_1, train_samples_2 = utils.gen_samples(n_samples, f1, f2)

    for k in k_list:
        knn_labels = knn.classify(samples, train_samples_1, train_samples_2, k)
        knn_prob_e = utils.get_error_probability(
            knn_labels, real_labels, f1["p"], f2["p"]
        )
        print(f"KNN classifier error prob (k={k}): {knn_prob_e:.2%}")


if __name__ == "__main__":

    k_list = [1, 11, 51]

    # tema 3)
    classify_all(k_list, config.t3)

    # tema 4)
    classify_all(k_list, config.t4)
