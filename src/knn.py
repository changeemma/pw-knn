import numpy as np
from tqdm import tqdm  # progress bar

# obtiene los k vecinos más cercanos a 'ref' de la lista 'x'
def get_neighbors(ref, x, k, indexed=False):
    if indexed:
        return sorted(enumerate(x), key=lambda xi: abs(xi[1] - ref))[:k]

    return sorted(x, key=lambda xi: abs(xi - ref))[:k]


# estima la densidad de probabilidad de 'x' mediante k-nn
def estimate(x, trn_x, k):
    n = len(trn_x)

    def _estimate(xi):
        knn = get_neighbors(xi, trn_x, k)
        vol = 2 * abs(xi - knn[-1])  # doble de la distancia al más lejano
        return k / (n * vol)

    return np.array([_estimate(xi) for xi in tqdm(x, desc="[info] knn estimation")])


# clasifica con la regla de k-nn
def classify(x, trn_x1, trn_x2, k):
    w1, w2 = 0, 1
    # junto ambas listas de entrenamiento
    trn_x = [*trn_x1, *trn_x2]
    # genero lista de etiquetas reales
    trn_labels = [w1] * len(trn_x1) + [w2] * len(trn_x2)

    def _classify(xi):
        knn = get_neighbors(xi, trn_x, k, indexed=True)
        knn_labels = [trn_labels[idx] for idx, _ in knn]
        nn_1 = sum(1 for nn_label in knn_labels if nn_label == w1)
        nn_2 = sum(1 for nn_label in knn_labels if nn_label == w2)
        return w1 if nn_1 >= nn_2 else w2

    return np.array([_classify(xi) for xi in tqdm(x, desc="[info] knn classification")])
