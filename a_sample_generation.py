from matplotlib import pyplot as plt

import src.config as config
import src.utils as utils

# a) Para dos distribuciones y probabilidades a priori dadas, genere N 1 = N 2 = 10^4 muestras de cada una.


def plot(cfg):
    x1_samples, x2_samples = utils.gen_samples(cfg["n_samples"], cfg["f1"], cfg["f2"])

    plt.figure(f"a_muestras_generadas_{cfg.get('name')}")
    plt.title(f"Muestras generadas {cfg.get('name')}")
    plt.hist(
        x1_samples,
        bins="auto",
        alpha=0.7,
        label=f"Uniforme [{cfg['f1']['a']}, {cfg['f1']['b']}]",
    )
    plt.hist(
        x2_samples,
        bins="auto",
        alpha=0.7,
        label=f"Gaussiana ({cfg['f2']['mu']}, {cfg['f2']['sigma']**2})",
    )
    plt.ylabel("Ocurrencias")
    plt.xlabel("Soporte")
    plt.legend()
    plt.grid(which="major", color="#DDDDDD", linewidth=0.8)
    plt.grid(which="minor", color="#EEEEEE", linestyle=":", linewidth=0.5)
    plt.minorticks_on()
    plt.show()


if __name__ == "__main__":
    # tema 3)
    plot(config.t3)

    # tema 4)
    plot(config.t4)
